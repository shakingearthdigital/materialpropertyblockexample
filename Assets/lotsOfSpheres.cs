﻿using UnityEngine;
using System.Collections;

public class lotsOfSpheres : MonoBehaviour {

	public GameObject spherePrefab;
	public int GridSize;

	private Renderer[] renderers;
	private MaterialPropertyBlock propertyBlock; 
	private float offset;
	static int _Color, _Glossiness, _Metallic;

	// Use this for initialization
	void Start () {
		renderers = new Renderer[GridSize*GridSize];
		int i = 0;
		for (int x = 0; x < GridSize; x++) {
			for (int z = 0; z < GridSize; z++) {
				GameObject sphere = (GameObject)Instantiate (spherePrefab, new Vector3 (x, 0, z), Quaternion.Euler(0,0,0));
				renderers[i] = sphere.GetComponent<Renderer> ();
				i++;
			}
		}

		// create new MaterialPropertyBlock which will be used by all objects
		propertyBlock = new MaterialPropertyBlock ();

		// If we're re-using the property block for the same materials only, we only need to get it once:
		renderers [0].GetPropertyBlock(propertyBlock);

		// Fetch the shader id for each property. This speeds up assignment
		_Color = Shader.PropertyToID("_Color");
		_Glossiness = Shader.PropertyToID("_Glossiness");
		_Metallic = Shader.PropertyToID("_Metallic");

	}
	
	// Update is called once per frame
	void Update () {
		int i = 0;
		Color color = new Color (1, 1, 1);
		float glossiness = 0;
		offset += Time.deltaTime;
		for (float x = 0; x < GridSize; x++) {
			for (float z = 0; z < GridSize; z++) {
				color.r = (1f - x / GridSize + offset) % 1;
				color.g = z / GridSize;
				glossiness = (z / GridSize + offset) % 1;


				// the slow way: 
//				renderers [i].material.SetColor ("_Color", color);
//				renderers [i].material.SetFloat ("_Glossiness", glossiness);
//				renderers [i].material.SetFloat ("_Metallic", glossiness);

				// the fast way:
				propertyBlock.SetColor (_Color, color);
				propertyBlock.SetFloat (_Glossiness, glossiness);
				propertyBlock.SetFloat (_Metallic, glossiness);
				renderers [i].SetPropertyBlock (propertyBlock);

				i++;
			}
		}
	}
}
