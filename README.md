# MaterialPropertyBlock Example
Using Unity 5.4.1

[See the code here!](https://bitbucket.org/shakingearthdigital/materialpropertyblockexample/src/01e7aec0ca5c5937661f508bfd9846b4b8d8a63c/Assets/lotsOfSpheres.cs?at=master&fileviewer=file-view-default)

This scene renders 2500 spheres using the same material with different properties.
![](https://bytebucket.org/shakingearthdigital/materialpropertyblockexample/raw/4d79e80fc33407b1f42418cdfbad4bd8ad576ea3/readme/render.PNG "Example render")

## Render cost
Setting 2 properties using the traditional material.SetFloat and material.SetColor. The circled 2.5k number shows that Unity is creating 2500 unique materials. This causes the large Material.SetPassUncached Time.
![](https://bytebucket.org/shakingearthdigital/materialpropertyblockexample/raw/4d79e80fc33407b1f42418cdfbad4bd8ad576ea3/readme/materialSet2Prop.PNG)

Setting 2 properties using a MaterialPropertyBlock. Notice that the number of materials dropped to 39, as all spheres now use the same material. The Material.SetPassUncached doesn't even show up in the profiler anymore.
![](https://bytebucket.org/shakingearthdigital/materialpropertyblockexample/raw/4d79e80fc33407b1f42418cdfbad4bd8ad576ea3/readme/blockSet2Prop.PNG)

## Script cost
Setting 3 properties using the traditional material set methods:
```
material.SetColor ("_Color", color)
material.SetFloat ("_Glossiness", glossiness)
material.SetFloat ("_Metallic", glossiness)
```
![](https://bytebucket.org/shakingearthdigital/materialpropertyblockexample/raw/4d79e80fc33407b1f42418cdfbad4bd8ad576ea3/readme/materialSet3PropScriptCost.PNG)

Setting 3 properties using a MaterialPropertyBlock. There's additional cost for setting the property, but overall it's still significantly faster.
```
propertyBlock.SetColor ("_Color", color);
propertyBlock.SetFloat ("_Glossiness", glossiness);
propertyBlock.SetFloat ("_Metallic", glossiness);
myRenderer.SetPropertyBlock (propertyBlock);
```
![](https://bytebucket.org/shakingearthdigital/materialpropertyblockexample/raw/4d79e80fc33407b1f42418cdfbad4bd8ad576ea3/readme/blockSet3PropScriptCost.PNG)

Additionally, there's a major speed gain for using shader property ids instead of property names. Ids can be fetched once using  `Shader.PropertyToID("propertyName");`

```
static int _Color, _Glossiness, _Metallic;

...

// store shader ids once
_Color = Shader.PropertyToID("_Color");
_Glossiness = Shader.PropertyToID("_Glossiness");
_Metallic = Shader.PropertyToID("_Metallic");

...

// use shader ids
propertyBlock.SetColor (_Color, color);
propertyBlock.SetFloat (_Glossiness, glossiness);
propertyBlock.SetFloat (_Metallic, glossiness);
```
![](https://bytebucket.org/shakingearthdigital/materialpropertyblockexample/raw/4d79e80fc33407b1f42418cdfbad4bd8ad576ea3/readme/blockSet3PropScriptCost_withIDs.PNG)
